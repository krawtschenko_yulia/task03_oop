package me.krawtschenko.yulia;

import me.krawtschenko.yulia.controller.FlowerShopController;

public class Application {
    public static void main(String[] args) {
        new FlowerShopController().startMenu();
    }
}
