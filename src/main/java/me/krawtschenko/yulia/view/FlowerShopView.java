package me.krawtschenko.yulia.view;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class FlowerShopView {
    private Scanner input;
    private HashMap<String, MenuItem> menu;

    public FlowerShopView() {
        input = new Scanner(System.in, "UTF-8");
        menu = new LinkedHashMap<String, MenuItem>();

        MenuItem buyFlower = new MenuItem("1", "Buy a single flower");
        menu.put("1", buyFlower);
        MenuItem buyManyFlowers = new MenuItem("2", "Buy several flowers");
        menu.put("2", buyManyFlowers);
        MenuItem buyBouquet = new MenuItem("3", "Buy a bouquet");
        menu.put("3", buyBouquet);
        MenuItem buyPottedPlant = new MenuItem("4", "Buy a plant");
        menu.put("4", buyPottedPlant);
        MenuItem quit = new MenuItem("q", "Quit");
        menu.put("q", quit);
    }

    public void addMenuItemAction(String key, MenuAction action) {
        MenuItem item = menu.get(key);
            item.linkAction(action);
        menu.put(key, item);
    }

    public String askFlowerName() {
        System.out.println("What kind of flower would you like?");
        String answer = input.nextLine().toLowerCase();

        return answer;
    }

    public int askNumberOfItems() {
        System.out.println("How many?");
        int answer = input.nextInt();
        input.nextLine();

        return answer;
    }

    public boolean askIfWantAnotherFlower() {
        System.out.println("Choose another flower? (y/n)");
        String answer = input.nextLine().toLowerCase();

        return answer.equals("y");
    }

    public String askPlantName() {
        System.out.println("What sort of plant would you like to purchase?");
        String answer = input.nextLine().toLowerCase();

        return answer;
    }

    public void displayMenu() {
        System.out.println("MENU");

        for (Map.Entry<String, MenuItem> option : menu.entrySet()) {
            System.out.printf("%s - %s%n", option.getValue().getKey(),
                    option.getValue().getDescription());
        }
    }

    public void awaitValidMenuOption() {
        String choice;
        do {
            choice = input.nextLine().toLowerCase();
        } while (!menu.keySet().contains(choice));

        menu.get(choice).getAction().execute();
    }

    public void displayTotal(double total) {
        System.out.println("Your total is: " + total);
    }

    public void printMessage(String message) {
        System.out.println(message);
    }
}

class MenuItem {
    private String key;
    private String description;
    private MenuAction action;

    MenuItem(String key, String description) {
        this.key = key;
        this.description = description;
    }

    public void linkAction(MenuAction action) {
        this.action = action;
    }

    public String getKey() {
        return key;
    }

    public String getDescription() {
        return description;
    }

    public MenuAction getAction() {
        return action;
    }
}
