package me.krawtschenko.yulia.model;

public abstract class FloweringPlant implements Merchandise {
    private String name;

    double price;
    private int availability;

    FloweringPlant(String name, double price, int availability) {
        this.name = name;
        this.price = price;
        this.availability = availability;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public int getAvailability() {
        return availability;
    }

    @Override
    public boolean isNotInStock() {
        return availability <= 0;
    }

    public void buy(int number) {
        availability -= number;
    }
}
