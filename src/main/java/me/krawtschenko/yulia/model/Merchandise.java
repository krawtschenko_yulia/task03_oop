package me.krawtschenko.yulia.model;

public interface Merchandise {
    String name = null;
    double price = 0.0;
    int availability = 0;
    
    void buy(int number);
    boolean isNotInStock();
    String getName();
    double getPrice();
    int getAvailability();
}
