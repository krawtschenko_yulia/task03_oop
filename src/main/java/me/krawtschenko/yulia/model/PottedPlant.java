package me.krawtschenko.yulia.model;

public class PottedPlant extends FloweringPlant {
    private boolean inBloom;

    public PottedPlant(String name, double price, int availability, boolean inBloom) {
        super(name, price, availability);

        this.inBloom = inBloom;
    }

    @Override
    public double getPrice() {
        return inBloom ? price * 1.5 : price;
    }
}
