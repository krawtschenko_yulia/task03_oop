package me.krawtschenko.yulia.model;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Flower extends FloweringPlant {
    private Date pickedAt;

    Flower(String name, double price, int availability, Date pickedAt) {
        super(name, price, availability);

        this.pickedAt = pickedAt;
    }

    public Flower(String name, double price, int availability) {
        super(name, price, availability);

        pickedAt = new Date();
    }

    @Override
    public double getPrice() {
        if (getTimeSincePicked() > TimeUnit.DAYS.toMillis(1)) {
            return price * 0.5;
        }
        return price;
    }

    private long getTimeSincePicked() {
        return (new Date().getTime() - pickedAt.getTime());
    }
}
