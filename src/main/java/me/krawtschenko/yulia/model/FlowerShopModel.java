package me.krawtschenko.yulia.model;

import java.util.ArrayList;
import java.util.HashMap;

public class FlowerShopModel {
    private ArrayList<Merchandise> stock;

    public FlowerShopModel() {
        stock = new ArrayList<>();
    }

    public void addToStock(Merchandise item) {
        stock.add(item);
    }

    public Merchandise getStockItemByName(String name)
            throws ItemNotInStockException {
        Merchandise found = null;

        for (Merchandise item : stock) {
            if (item.getName().equals(name)) {
                found = item;
            }
        }
        if (found == null) {
            throw new ItemNotInStockException("No such item: " +
                    name);
        }

        return found;
    }

    public void buy(Merchandise item) throws
            ItemNotInStockException, InsufficientStockException {
        buy(item, 1);
    }

    public void buy(Merchandise item, int number) throws
            ItemNotInStockException, InsufficientStockException {
        if (item.isNotInStock()) {
            throw new ItemNotInStockException("No such item: " +
                    item.getName());
        }
        if (number > item.getAvailability()) {
            throw new InsufficientStockException("Not enough items in stock.");
        }
        item.buy(number);
    }

    public void buyBouquet(HashMap<Merchandise, Integer> map) throws
            ItemNotInStockException, InsufficientStockException {
        for (Merchandise flower : map.keySet()) {
            if (flower.isNotInStock() || !(flower instanceof Flower)) {
                throw new ItemNotInStockException("No such item: " +
                        flower.getName());
            }
        }
        for (HashMap.Entry<Merchandise, Integer> entry : map.entrySet()) {
            if (entry.getKey().getAvailability() < entry.getValue()) {
                throw new InsufficientStockException("Not enough "
                        + entry.getKey().getName() + " in stock.");
            }
        }

        for (HashMap.Entry<Merchandise, Integer> entry : map.entrySet()) {
            ((Flower) entry.getKey()).buy(entry.getValue());
        }
    }

    public double getTotal(Merchandise item) {
        return item.getPrice();
    }

    public double getTotal(Merchandise item, int number) {
        return item.getPrice() * number;
    }

    public double getTotal(HashMap<Merchandise, Integer> bouquet) {
        double total = 0;

        for (HashMap.Entry<Merchandise, Integer> entry : bouquet.entrySet()) {
            double price = ((Flower) entry.getKey()).getPrice();
            total += price * entry.getValue();
        }

        return total;
    }
}