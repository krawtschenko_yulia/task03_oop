package me.krawtschenko.yulia.model;

public class InsufficientStockException extends Exception {
    InsufficientStockException(String message) {
        super(message);
    }
}
