package me.krawtschenko.yulia.model;

public class ItemNotInStockException extends Exception {
    ItemNotInStockException(String message) { super(message); }
}
