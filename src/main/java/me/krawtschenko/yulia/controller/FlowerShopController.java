package me.krawtschenko.yulia.controller;

import me.krawtschenko.yulia.model.*;
import me.krawtschenko.yulia.view.FlowerShopView;
import me.krawtschenko.yulia.view.MenuAction;

import java.util.HashMap;
import java.util.LinkedHashMap;


public class FlowerShopController {
    private FlowerShopModel model;
    private FlowerShopView view;

    private MenuAction buyOneFlower;
    private MenuAction buyManyFlowers;
    private MenuAction buyBouquet;
    private MenuAction buyPottedPlant;
    private MenuAction quit;

    public FlowerShopController() {
        model = new FlowerShopModel();
        view = new FlowerShopView();

        Merchandise rose = new Flower("rose", 5.5, 17);
        model.addToStock(rose);
        Merchandise orchid = new PottedPlant("orchid", 10, 6, true);
        model.addToStock(orchid);
        Merchandise carnation = new Flower("carnation", 3.2, 12);
        model.addToStock(carnation);

        buyOneFlower = new MenuAction() {
            @Override
            public void execute() {
                String name = view.askFlowerName();
                try {
                    Merchandise item = model.getStockItemByName(name);
                    if (item instanceof Flower) {

                        model.buy(item);

                        double total = model.getTotal(item);
                        view.displayTotal(total);
                    } else {
                        view.printMessage("No such flower in stock.");
                    }
                } catch (ItemNotInStockException e) {
                    view.printMessage("Item not available.");
                    e.printStackTrace();
                } catch (InsufficientStockException e) {
                    view.printMessage("Insufficient stock.");
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        view.addMenuItemAction("1", buyOneFlower);

        buyManyFlowers = new MenuAction() {
            @Override
            public void execute() {
                String name = view.askFlowerName();
                int number = view.askNumberOfItems();
                try {
                    Merchandise item = model.getStockItemByName(name);
                    if (item instanceof Flower) {

                        model.buy(item, number);

                        double total = model.getTotal(item);
                        view.displayTotal(total);
                    } else {
                        view.printMessage("No such flower in stock.");
                    }

                    double total = model.getTotal(item, number);
                    view.displayTotal(total);
                } catch (ItemNotInStockException e) {
                    view.printMessage("Item not available.");
                    e.printStackTrace();
                } catch (InsufficientStockException e) {
                    view.printMessage("Insufficient stock.");
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        view.addMenuItemAction("2", buyManyFlowers);

        buyBouquet = new MenuAction() {
            @Override
            public void execute() {
                HashMap<Merchandise, Integer> map = new LinkedHashMap<>();
                String name;
                int number;

                try {
                    do {
                        name = view.askFlowerName();
                        number = view.askNumberOfItems();

                        Merchandise item = model.getStockItemByName(name);
                        if (item instanceof Flower) {
                            map.put(item, number);
                        } else {
                            view.printMessage("No such flower in stock.");
                        }
                    } while (view.askIfWantAnotherFlower());

                    model.buyBouquet(map);

                    double total = model.getTotal(map);
                    view.displayTotal(total);
                } catch (ItemNotInStockException e) {
                    view.printMessage("Item not available.");
                    e.printStackTrace();
                } catch (InsufficientStockException e) {
                    view.printMessage("Insufficient stock.");
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        view.addMenuItemAction("3", buyBouquet);

        buyPottedPlant = new MenuAction() {
            @Override
            public void execute() {
                String name = view.askPlantName();
                try {
                    Merchandise item = model.getStockItemByName(name);
                    if (item instanceof Flower) {

                        model.buy(item);

                        double total = model.getTotal(item);
                        view.displayTotal(total);
                    } else {
                        view.printMessage("No such plant in stock.");
                    }
                } catch (ItemNotInStockException e) {
                    view.printMessage("Item not available.");
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        view.addMenuItemAction("4", buyPottedPlant);

        quit = new MenuAction() {
            @Override
            public void execute() {
                System.exit(0);
            }
        };
        view.addMenuItemAction("q", quit);
    }

    public void startMenu() {
        view.displayMenu();
        view.awaitValidMenuOption();
    }
}